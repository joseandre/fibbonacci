                       **RELATÓRIO**
                       
**Funcionalidade do programa:** Este programa tem a funcionalidade de cálcular e exibir os elementos da sequência de Fibonacci até
    o valor limite estipulado pelo usuário.



**Funcionalidades das funções:**

**printArray;** Esta função faz a impressão dos elementos do ponteiro que é passado como parâmetro ao chama-la.

**fib;** Esta função tem a funcionalidade de retornar a quantidade de elementos que o ponteiro vai ter, ela também tem a
    função de calcular os elementos e armazenar eles dentro do ponteiro que é passado por parâmetro ao chamá-la.
    
**main;** Esta é a função principal do programa, nela são declaradas as funções "L" que recebe o valor limite que o 
    usuário escolhe e a variável "*A", que é um ponteiro que vai conter os elementos da sequência de Fibonacci até o 
    valor limite. Esta função que chama as funções fib e printArray para que o programa rode corretamente.



**Modificações dos erros e implementações:**

**printArray;** Nesta função foi modificado a condição de parada dentro de for, que ia até "<=_sz", para "<_sz".

**fib;** Nesta função eu dei o valor de 1 para a variável fib1 e retirei a variável "fib2", pois utilizei um cálculo que não necessitava de três variáveis, 
    também implementei um função de condição (while) que dentro desta função é onde ocorre a parte de inserir os elementos 
    dentro do ponteiro que é passado por referencia quando a função é chamada, o "break" foi adicionado pois ele se faz necessário, 
    sem ele o programa entra em um loop infinito dentro do while. Uma coisa que me fez quebrar a cabeça e eu findei sem conseguir solucionar,
    é o fato do programa dar erro se eu retirar o while e deixar que os valores sejam inseridos dentro do ponteiro, este foi um erro
    que eu não consegui solucionar e então deixei o while para que o programa rodasse.

**main;** Nesta função eu fiz a alocação dinamica de "A", me basenado no tamanho se "sz", assim eu aloquei o espaço necessário para os elementos 
    que seriam inseridos no ponteiro "A". Para armazenar os elementos dentro de "A" eu chamei novamente a função fib e passei "L" e 
    "A"(já estando alocado dinamicamente).